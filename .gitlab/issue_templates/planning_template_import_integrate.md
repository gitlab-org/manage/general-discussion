## Team

~"group::import and integrate"
* 9 BE: @carlad-gl, @georgekoltsov, @Imjaydip, @jnutt, @knejad, @.luke, @reza-marandi, @rodrigo.tomonari, @SamWord
* 2 FE: @justin_ho, @obaiye   
* 1 shared SEC: @ameyadarshan
* TW: @ashrafkhamis 
* SSC: @anton
* EM: @wortschi 
* PM: @m_frankiewicz 

## Capacity / OOO

| Team | Weight |
|------|--------|
| ~backend |   |
| ~3412464 |   |

## Anticipated % split for features/bugs/maintenance 

* 40% features (weight)
* 30% bugs (weight)
* 30% maintenance (weight)

## Board

* [Import and Integrate Current Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/1459244?milestone_title=Upcoming&label_name%5B%5D=group%3A%3Aimport%20and%20integrate)

## High level Objectives / Progress on quarterly OKRs


## Objectives


### Product prioritized ~"type::feature" list

1. Any %"17.x" carryover
   1. Not started
      - _item_
   2. At risk of slipping
      - _item_
2. New feature work
   - Direct transfer
     - _item_
   - 3rd party importers
     - _item_
   - Integrations/Webhooks
     - _item_

### Engineering prioritized ~"type::maintenance" list

1. Any %"17.x" carryover
   1. Not started
     - _item_
   2. At risk of slipping
     - _item_
2. New maintenance work
   - Direct transfer
     - _item_
   - 3rd party importers
     - _item_
   - Integrations/Webhooks
     - _item_
   - Other (e.g. Cells)
     - _item_

### Prioritized ~"type::bug" list

1. Any %"17.x" carryover
   1. Not started
     - _item_
   2. At risk of slipping
     - _item_
2. New bugs
   - Security
     - _item_
   - UX debt
     - _item_
   - Direct transfer
     - _item_
   - 3rd party importers
     - _item_
   - Integrations/Webhooks
     - _item_
   - Other
     - _item_

## Release Post Items

| Status | Issue | Release Post MR |
| ---- | ---- | ---- |
|      |      |      |
|      |      |      |


## TODO
* [ ]  Write milestone objectives.
* [ ]  Discuss capacity (OOO plans).
* [ ]  Review the roadmap and milestone objectives with the team.
* [ ]  Issues that can be delivered in 17.x, label with priority label ~"milestone::x" and set milestone to `17.x`.
* [ ]  Publish the planning call [video](url). [Group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpMH2PN23rAJw1pnJT9AJs1).
* [ ]  Link planning issue for the next milestone to this one.

[< Previous]() | [Next >]()

/label ~"group::import and integrate" ~"Planning Issue" ~"type::ignore" 

/assign @wortschi @m_frankiewicz 