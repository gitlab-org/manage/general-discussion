## {REPLACE_WITH_MILESTONE} Planning for {REPLACE_WITH_GROUP}

* Board link: {REPLACE_WITH_BOARD_LINK}

--------------------------------------------------------------------------------

Internal Milestone {REPLACE_WITH_MILESTONE} Review & Discussion 🚀
{REPLACE_WITH_MILESTONE} Milestone: 2022-xx-18 to 2022-xx-17

# Capacity


| Team | Weight |
| ------ | --------- |
| ~backend | |
| ~frontend | |

## Objectives

<!-- Problems we're trying to solve in this release and why they matter. -->

Themes
* 🏎 Performance
* 🔒 Security
* 💼 GitLab.com Enterprise Readiness
* 🔽 Workspace Settings Inheritance
* 😍 Usability
* 📈 Customer Requests
* 🛠 Engineering Allocation
* 🏃🏾‍♀️ Rapid Action
* 🏆 OKR 

## Product Themes

* [GitLab Hosted First](https://about.gitlab.com/direction/#gitlab-hosted-first) :cloud: 
* [Improve Key workflow usability](https://about.gitlab.com/direction/#improve-key-workflow-usability) :key: 
* [Extend our lead in CI/CD](https://about.gitlab.com/direction/#extend-our-lead-in-cicd) :medal: 


| [Title](Link) | Weight | Theme | Priority | Note |
|-------|--------|-------| ------- | ----- |

## Research/Needs Weight 🕵🏻‍♂️

## Defining Success

<!-- The metrics or other data points you're using in this release to guide priorities. -->
<!-- Prioritized issues should always consider IACV, your group's North Star, and your group's direction/vision. -->
<!-- e.g. "These features support progress towards Dev Section's Top 2 IACV Q1FY21 goals." -->

## To-Do
- [ ] Define an approximate scope for the coming release.
- Apply appropriate labels to issues:
  - [ ] Priority
  - [ ] Department
  - [ ] Pricing Tier
- [ ] Review the proposed scope with Engineering.
- [ ] Review design objectives with UX.
- [ ] Get feedback and estimates from Engineering, confirming scope for the release.
- [ ] Final verification of issues scheduled for the release.
- [ ] Present selected issues in the Manage Kickoff on the 17th!

[Previous](#) <=  => [Next](#)

/label ~"Planning Issue" 
