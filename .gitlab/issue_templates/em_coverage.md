## :handshake: Responsibilities

| Priority | Theme | Context Link | Primary | Secondary |
| -------- | ----- | ------------ | ------- | --------- |
| HIGH     | ...   |              |         |           | 
| Medium   | ...   |              |         |           | 

## :white_check_mark: Issue Tasks

### :o: Opening Tasks
- [ ] Assign to yourself
- [ ] Title the issue `EM Coverage for YOUR-NAME from XXXX-XX-XX until XXXX-XX-XX`
- [ ] Add an issue comment for your :recycle: Retrospective Thread
- [ ] Add any relevant references including direction pages, group handbook pages, etc
- [ ] Fill in the Responsibilities table with broad based responsibilities
- [ ] Fill in the specific Coverage Tasks with distinct items to complete and assignees
- [ ] Assign to anyone with a specific task or responsibility assigned to them
- [ ] Share this issue in your section, stage and group Slack channels
- [ ] Ensure you've assigned tasks via PTO Ninja including assigning some tasks to a relevant #g_ , #s_ or #product slack channel
- [ ] Ensure your PTO Ninja auto-responder points team members to this issue
- [ ] Update your [GitLab status](https://docs.gitlab.com/ee/user/profile/#current-status) to include a reference to this issue

### :x: Closing Tasks
- [ ] Review the [Returning from Time Off ](https://about.gitlab.com/handbook/product/product-manager-role/#returning-from-time-off) guidance
- [ ] Assign back to yourself and remove others
- [ ] Review any Retrospective Items and update [this template](https://gitlab.com/gitlab-com/Product/-/blob/master/.gitlab/issue_templates/PM-Coverage.md)
